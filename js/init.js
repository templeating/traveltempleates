(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.modal-trigger').leanModal();
    $('.datepicker').pickadate({
      selectMonths: true, // Creates a dropdown to control month
      selectYears: 15 // Creates a dropdown of 15 years to control year
    });

    $('#filters-selects .clean-input i').click(function () {
      var input_id = $(this).data('input-id');
      $('#'+input_id).val('');
    })

    $('#btn-search').click(function () {
      App.submit('form-search', 'list.html')
    });

    $('table#air-list-price tr').click(function () {
      var id_flights = $(this).data('id-flights')
      location.href = 'form-data.html'
    });

    $('.selected-from').select2({
      placeholder: "From",
      allowClear: true
    });

    $(".selected-from").on("select2:open", function (e) {
      $("#icon-from").attr('style','color: #26a69a;');
      $("#icon-from-filter").attr('style','color: #26a69a;');
    });

    $(".selected-from").on("select2:close", function (e) {
      var value = $('.selected-from :selected').text();
      if(value !=""){
        $("#icon-from").attr('style','color: #26a69a;');
        $("#icon-from-filter").attr('style','color: #26a69a;');
      }
      else {
        $("#icon-from").attr('style','color: #727272;');
        $("#icon-from-filter").attr('style','color: #727272;');
      }
    });

    $('.selected-to').select2({
      placeholder: "From",
      allowClear: true
    });

    $(".selected-to").on("select2:open", function (e) {
      $("#icon-to").attr('style','color: #26a69a;');
      $("#icon-to-filter").attr('style','color: #26a69a;');
    });

    $(".selected-to").on("select2:close", function (e) {
      var value = $('.selected-to :selected').text();
      if(value !=""){
        $("#icon-to").attr('style','color: #26a69a;');
        $("#icon-to-filter").attr('style','color: #26a69a;');
      }
      else {
        $("#icon-to").attr('style','color: #727272;');
        $("#icon-to-filter").attr('style','color: #727272;');
      }
    });

  }); // end of document ready
})(jQuery); // end of jQuery name space